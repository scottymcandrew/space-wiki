FROM python:3.9.6

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

ENV PORT 80
EXPOSE 80

ENTRYPOINT [ "python3" ]
CMD [ "./app.py" ]